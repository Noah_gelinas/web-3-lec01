#comments are written using hastag.
"""
multi line comments
"""


counter = 0 #dont assign datatype. (like js) depends on value 

print(counter)

counter = 'hello'

counter=0

score_total=0 #snake_case
testScore=0  #camelCase


while testScore!=-1 :
    #indented means it is child 
    testScore = int(input("Enter your test score"))
    if testScore>= 0 and testScore <=100:
        score_total+=testScore
        #print("inside if")        
        counter+=1
    
    #print("outside if but inside while")

print("Total Score: " + str(score_total))
print(f'amount of executions {counter}')
#print("outside if and while")

