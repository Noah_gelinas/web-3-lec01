import math
import random
def exercise1():
    dividend=int(input("Enter a number"))
    divisor=int(input("Enter a number"))
    if dividend % 2 == 0 :
        if dividend%4==0:
            print(f'{dividend} is even and a multiple of 4')
        else:
            print(f'{dividend} is even and not a multiple of 4')
    elif dividend % 2 == 1:
        if dividend%4==0:
            print(f'{dividend} is odd and a multiple of 4')
        else:
            print(f'{dividend} is odd and not a multiple of 4')
    
    if dividend%divisor==0:
        print(f'{dividend} divides evenly by {divisor}')
    else:
        print(f'{dividend} does not divide evenly by {divisor}')

def exercise2():
    name=input("Enter your name ")
    print(f'Hi {name}, how old are you?')
    age=int(input("Enter your age"))
    gender=input("Enter your gender")
    year_100years=2024+ (100-age)

    print(f'So, mr/ms {name}, you will be 100 years old on {year_100years}.')

    num=int(input("Enter a number between 3 and 13"))

    if num > 13 and num < 3 : 
        num=int(input("Enter a number between 3 and 13"))
    
def exercise3():
    name=input("Enter your name ")
    print(f'Hi {name}, Enter your score [0-100].')
    score=int(input())
    grade=0
    comment=""

    if score >=95:
        grade='A'
        comment="Brilliant"
    elif score>=80:
        grade="B" 
        comment="Excellent"
    elif score>=70:
        grade="C"
        comment="Good job"
    elif score>=60:
        grade="D"
        comment="Medium"
    else:
        grade="F"
        comment="Fail"


    print(f'Yo {name}, you got score which is an/a {grade}, {comment}')

def exercise4():
    cont = True
    while cont:
        number=int(input("Enter a number"))
        squareRoot=math.sqrt(number)
        print(squareRoot)
    
        ans= input("Would you like to continue? Enter True or False")
        if ans == "False":
            cont=False

def exercise5():
    number=int(input('Enter number between [1-100]'))
    counter=0

    rand=random.randint(1,100)
    while rand != number:
        rand=random.randint(1,100)
        print(f'{rand} is the random number')
        counter+=1
    
    print(counter)

def exercise6():
    positive = int(input("Enter a number greater than one "))
    num=0
    num1=1
    total=0

    for i in range(positive):
        print(num)
        total=num+num1
        num=num1
        num1=total




    
#exercise1()
#exercise2()
#exercise3()
#exercise4()
#exercise5()
exercise6()